import statistics
from io import StringIO

import numpy as np
import pandas as pd
import xlsxwriter
from sklearn.linear_model import LinearRegression
from xlsxwriter.worksheet import Worksheet

key_id = 'id'
key_name = 'name'
key_sex = 'sex'
key_age = 'age'
key_height = 'height'
key_weight = 'weight'
key_team = 'team'
key_noc = 'noc'
key_games = 'games'
key_year = 'year'
key_season = 'season'
key_city = 'city'
key_sport = 'sport'
key_event = 'event'
key_medal = 'medal'

attr_types = {
    key_id: 'int',
    key_name: 'str',
    key_sex: 'str',
    key_age: 'int',
    key_height: 'int',
    key_weight: 'float',
    key_team: 'str',
    key_noc: 'str',
    key_games: 'str',
    key_year: 'int',
    key_season: 'str',
    key_city: 'str',
    key_sport: 'str',
    key_event: 'str',
    key_medal: 'str'
}


def get_missing_count(data_m: list) -> int:
    missing_count = 0
    for i, item in enumerate(data_m):
        if pd.isnull(item):
            missing_count += 1
    return missing_count


def closest_value(input_list, input_value):
    arr = np.asarray(input_list)
    i = (np.abs(arr - input_value)).argmin()
    return arr[i]


def get_outers(data_o: list) -> list:
    data_o.sort()
    q2 = statistics.median(data_o)
    print(f'  Median (Q2): {str(q2)}')
    val = closest_value(data_o, q2)
    q2_index = len(data_o) - 1 - data_o[::-1].index(int(val))
    q1 = statistics.median(data_o[:q2_index])
    print(f'  Q1: {str(q1)}')
    q3 = statistics.median(data_o[q2_index:])
    print(f'  Q3: {str(q3)}')
    lower_bound = q1 - (q3 - q1) * 1.5
    print(f'  Lower bound: {str(lower_bound)}')
    upper_bound = q3 + (q3 - q1) * 1.5
    print(f'  Lower bound: {str(upper_bound)}')
    outliers = [item for item in data_o if item < lower_bound or item > upper_bound]
    print(f'  Outliers:\n  {str(outliers)}')
    return outliers


def write_descr_stat_num(worksheet: Worksheet, row_num: int, key: str, values: list) -> None:
    worksheet.write(row_num, 0, key)
    values_without_nones = [item for item in values if not pd.isnull(item)]
    min_val = min(values_without_nones)
    print(f'key\nMin {key}s: {str(min_val)}')
    worksheet.write(row_num, 2, min_val)
    max_val = max(values_without_nones)
    print(f'Max {key}s: {str(max_val)}')
    worksheet.write(row_num, 3, max_val)
    mean_val = statistics.mean(values_without_nones)
    print(f'Mean {key}s: {str(mean_val)}')
    worksheet.write(row_num, 4, mean_val)
    missing_vals = get_missing_count(values)
    print(f'Missing {key}s: {str(missing_vals)}')
    worksheet.write(row_num, 5, missing_vals)
    outliers_vals = get_outers(values_without_nones)
    print(f'Outliers {key}s: {str(len(outliers_vals))}\n{"-":-^100}')
    worksheet.write(row_num, 6, str(len(outliers_vals)))


def get_frequencies(data_f: list[str]) -> dict[str, int]:
    data_without_nones = []
    for item in data_f:
        if not pd.isnull(item):
            data_without_nones.append(item)
    freq_dict = {}
    for i, item in enumerate(data_without_nones):
        if item in freq_dict.keys():
            freq_dict[item] = freq_dict.get(item) + 1
        else:
            freq_dict[item] = 1
    return freq_dict


def write_descr_stat_str(worksheet: Worksheet, row_num_stat: int, col_num_data: int, chart_cell: str, key: str) -> None:
    worksheet.write(row_num_stat, 0, key)
    values = df[key].tolist()
    values_frequencies = dict(sorted(get_frequencies(values).items(), key=lambda item: item[1], reverse=True))
    print('key\nFrequencies:')
    freqs_str = str(values_frequencies)
    print(f'  {freqs_str[:1000]}{"..." if len(freqs_str) > 1000 else ""}')
    chart = workbook.add_chart({'type': 'column'})
    worksheet.write(20, col_num_data, key)
    for i, freq_key in enumerate(values_frequencies, start=21):
        worksheet.write(i, col_num_data, freq_key)
        worksheet.write(i, col_num_data + 1, values_frequencies.get(freq_key))
    col_char_1 = xlsxwriter.utility.xl_col_to_name(col_num_data)
    col_char_2 = xlsxwriter.utility.xl_col_to_name(col_num_data + 1)
    chart.add_series({
        'categories': '=\'Descriptive statistics\'!' + col_char_1 + '$22:$' + col_char_1 + '$' + str(21 + len(values_frequencies)),
        'values': '=\'Descriptive statistics\'!$' + col_char_2 + '$22:$' + col_char_2 + '$' + str(21 + len(values_frequencies)),
        'name': key
    })
    worksheet.insert_chart(chart_cell, chart)
    missing_values = get_missing_count(values)
    print(f'Missing {key}s: {str(missing_values)}')
    worksheet.write(row_num_stat, 3, missing_values)
    values_freq_without_nones = [item for item in list(values_frequencies.values()) if item is not None]
    if len(values_freq_without_nones) > 4:
        outliers_vals = get_outers(values_freq_without_nones)
    else:
        outliers_vals = []
    print(f'Outliers {key}s: {str(len(outliers_vals))}\n{"-":-^100}')
    worksheet.write(row_num_stat, 4, str(len(outliers_vals)))


def prepare_date(df: pd.DataFrame, key: str) -> pd.DataFrame:
    values_without_nones = [item for item in df[key] if not pd.isnull(item)]
    mean_age = statistics.mean(values_without_nones)
    df_prepared = pd.DataFrame(df)
    df_prepared[key] = df_prepared[key].replace(np.nan, mean_age)
    values_without_nones.sort()
    q2 = statistics.median(values_without_nones)
    val = closest_value(values_without_nones, q2)
    q2_index = len(values_without_nones) - 1 - values_without_nones[::-1].index(int(val))
    q1 = statistics.median(values_without_nones[:q2_index])
    q3 = statistics.median(values_without_nones[q2_index:])
    lower_bound = q1 - (q3 - q1) * 1.5
    upper_bound = q3 + (q3 - q1) * 1.5
    df_prepared = df_prepared.drop(df_prepared[(df_prepared[key] < lower_bound) | (df_prepared[key] > upper_bound)].index)
    return df_prepared


def predict_value(df: pd.DataFrame, key: str, year_pred: int, x_year: np.ndarray, worksheet: Worksheet, col_num: int, chart_cell: str) -> None:
    y = np.array(df[df[key_year] < year_pred][key].to_list())
    model = LinearRegression().fit(x_year, y)
    r2 = model.score(x_year, y)
    pred = model.predict(np.array([year_pred]).reshape(-1, 1))[0]
    fact = df[df[key_year] == year_pred][key].iloc[0]
    abs_error = abs(pred - fact)
    rel_error = abs_error / fact * 100
    print(
        f'{key.capitalize()}\n{key.capitalize()}s: {y}\nR2: {r2}\nPredicted in {year_pred}: {pred}\nFact in {year_pred}: {fact.tolist()}\nAbsolute error: {abs_error}\nRelative error: {rel_error}%\n{"-":-^100}')

    worksheet.write(20, col_num, 'Year')
    worksheet.write(20, col_num + 1, key.capitalize())
    worksheet.write(20, col_num + 2, 'Prediction')
    worksheet.write(20, col_num + 3, 'Absolute error')
    worksheet.write(20, col_num + 4, 'Relative error, %')
    df = df.reset_index()
    for i in range(0, len(df)):
        worksheet.write(i + 21, col_num, df.iloc[i][key_year])
        worksheet.write(i + 21, col_num + 1, df.iloc[i][key])
    worksheet.write(len(df) + 20, col_num + 2, pred)
    worksheet.write(21, col_num + 3, abs_error)
    worksheet.write(21, col_num + 4, rel_error)
    chart = workbook.add_chart({'type': 'line'})
    col_char_1 = xlsxwriter.utility.xl_col_to_name(col_num)
    col_char_2_1 = xlsxwriter.utility.xl_col_to_name(col_num + 1)
    col_char_2_2 = xlsxwriter.utility.xl_col_to_name(col_num + 2)
    chart.add_series({
        'categories': '=\'Prediction\'!' + col_char_1 + '$22:$' + col_char_1 + '$' + str(22 + len(y)),
        'values': '=\'Prediction\'!$' + col_char_2_1 + '$22:$' + col_char_2_1 + '$' + str(22 + len(y)),
        'marker': {'type': 'automatic'},
        'data_labels': {'value': True, 'position': 'above', 'num_format': '# ###.0'},
        'name': 'Fact'
    })
    chart.add_series({
        'categories': '=\'Prediction\'!' + col_char_1 + '$22:$' + col_char_1 + '$' + str(22 + len(y)),
        'values': '=\'Prediction\'!$' + col_char_2_2 + '$22:$' + col_char_2_2 + '$' + str(22 + len(y)),
        'marker': {'type': 'automatic'},
        'data_labels': {'value': True, 'position': 'right', 'num_format': '# ###.0'},
        'name': 'Predicted'
    })
    chart.set_size({'width': 900, 'height': 350})
    worksheet.insert_chart(chart_cell, chart)


################################################## PARSE SOURCE DATA ##################################################
print(f'\n\n{" PARSE SOURCE DATA ":-^100}')
file_name = 'athlete_events.csv'
data = ''
with open(file_name, 'r') as file:
    data = file.read().replace(';', '').replace('"\n"', '\n').replace('""', '"')
print(f'Read file {file_name}\n{"-":-^100}')

data = data[1:-1]

attrs = [item.replace('"', '').lower() for item in data[:data.find('\n')].split(',')]
print(f'Got attributes from file data: \n{attrs}\n{"-":-^100}')

data = data[data.find('\n') + 1:-1]

df = pd.read_csv(StringIO(data), dtype=str, delimiter=',', quotechar='"', keep_default_na=False, names=attrs)
df = df.replace('NA', np.nan)
df[key_id] = pd.to_numeric(df[key_id], errors='coerce')
df[key_age] = pd.to_numeric(df[key_age], errors='coerce')
df[key_height] = pd.to_numeric(df[key_height], errors='coerce')
df[key_weight] = pd.to_numeric(df[key_weight], errors='coerce')
df[key_year] = pd.to_numeric(df[key_year], errors='coerce')

print(f'Parsed csv data')
workbook = xlsxwriter.Workbook('result.xlsx')

################################################## SOURCE DATA ##################################################
# print(f'\n\n{" SOURCE DATA ":-^100}')
# worksheet1 = workbook.add_worksheet()
# worksheet1.name = 'Source data'
# for i, row in df.iterrows():
#     for j, attr in enumerate(attrs):
#         worksheet1.write(i, j, row[attr] if not pd.isnull(row[attr]) else None)
# print(f'Wrote source data to xlsx')

################################################## ATTRIBUTES ##################################################
print(f'\n\n{" ATTRIBUTES ":-^100}')
worksheet2 = workbook.add_worksheet()
worksheet2.name = 'Attributes'
worksheet2.write(0, 0, 'Attribute')
worksheet2.write(0, 1, 'Type')
for i, attr_key in enumerate(attr_types.keys(), start=1):
    worksheet2.write(i, 0, attr_key)
    worksheet2.write(i, 1, attr_types.get(attr_key))
print(f'Wrote attributes to xlsx')

################################################## DESCRIPTIVE STATISTICS ##################################################
print(f'\n\n{" DESCRIPTIVE STATISTICS ":-^100}')
worksheet3 = workbook.add_worksheet()
worksheet3.name = 'Descriptive statistics'
worksheet3.write(0, 0, 'Name')
worksheet3.write(0, 1, 'N')
worksheet3.write(0, 2, 'Min')
worksheet3.write(0, 3, 'Max')
worksheet3.write(0, 4, 'Mean')
worksheet3.write(0, 5, 'Missing')
worksheet3.write(0, 6, 'Outliers')

n = len(df)
print(f'N: {str(n)}\n{"-":-^100}')
for i in range(1, 5):
    worksheet3.write(i, 1, n)

write_descr_stat_num(worksheet3, 1, key_id, df[key_id].tolist())
write_descr_stat_num(worksheet3, 2, key_age, df[key_age].tolist())
write_descr_stat_num(worksheet3, 3, key_height, df[key_height].tolist())
write_descr_stat_num(worksheet3, 4, key_weight, df[key_weight].tolist())

worksheet3.write(6, 0, 'Name')
worksheet3.write(6, 1, 'N')
worksheet3.write(6, 2, 'Frequency')
worksheet3.write(6, 3, 'Missing')
worksheet3.write(6, 4, 'Outliers')

for i in range(7, 18):
    worksheet3.write(i, 1, n)
    worksheet3.write(i, 2, 'See the graph')

write_descr_stat_str(worksheet3, 7, 8, 'I2', key_name)
write_descr_stat_str(worksheet3, 8, 16, 'Q2', key_sex)
write_descr_stat_str(worksheet3, 9, 24, 'Y2', key_team)
write_descr_stat_str(worksheet3, 10, 32, 'AG2', key_noc)
write_descr_stat_str(worksheet3, 11, 40, 'AO2', key_games)
write_descr_stat_str(worksheet3, 12, 48, 'AW2', key_year)
write_descr_stat_str(worksheet3, 13, 56, 'BE2', key_season)
write_descr_stat_str(worksheet3, 14, 64, 'BM2', key_city)
write_descr_stat_str(worksheet3, 15, 72, 'BU2', key_sport)
write_descr_stat_str(worksheet3, 16, 80, 'CC2', key_event)
write_descr_stat_str(worksheet3, 17, 88, 'CK2', key_medal)

print(f'Wrote descriptive statistics to xlsx')

################################################## QUESTIONS ANSWERS ##################################################
print(f'\n\n{" QUESTIONS ANSWERS ":-^100}')
worksheet4 = workbook.add_worksheet()
worksheet4.name = 'Questions'
text_wrap_format = workbook.add_format({'text_wrap': True})
worksheet4.set_column(0, 0, 3)
worksheet4.set_column(1, 1, 125)
worksheet4.set_column(2, 2, 100)
worksheet4.set_row(3, 29)

worksheet4.write(0, 0, '1')
worksheet4.write(0, 1, 'Сколько лет было самому молодому мужчине и женщине-участникам Олимпийских игр 1992 года?')
men_ages = [item for item in df[df[key_sex] == 'M'][key_age].tolist() if not pd.isnull(item)]
q1_answer_men = min(men_ages)
women_ages = [item for item in df[df[key_sex] == 'F'][key_age].tolist() if not pd.isnull(item)]
q1_answer_women = min(women_ages)
print(f'Question 1 answer:\n  Youngest man: {q1_answer_men}\n  Youngest women: {q1_answer_women}\n{"-":-^100}')
worksheet4.write(0, 2, f'Мужчина: {q1_answer_men}. Женщина: {q1_answer_women}.')

worksheet4.write(1, 0, '2')
worksheet4.write(1, 1, 'Каков был процент баскетболистов мужского пола среди всех мужчин-участников Олимпийских игр 2012 года?')
df_2012_men = df[(df[key_year] == 2012) & (df[key_sex] == 'M')].drop_duplicates(subset=[key_name])
n_2012_men = len(df_2012_men)
q2_answer = round(len(df_2012_men[df_2012_men[key_sport] == 'Basketball']) / n_2012_men * 100, 1)
print(f'Question 2 answer: {q2_answer}\n{"-":-^100}')
worksheet4.write(1, 2, q2_answer)

worksheet4.write(2, 0, '3')
worksheet4.write(2, 1, 'Каковы среднее и стандартное отклонение роста теннисисток, участвовавших в Олимпийских играх 2000 года?')
df_2000_women_tennis_heights = pd.to_numeric(
    df[(df[key_year] == 2000) & (df[key_sex] == 'F') & (df[key_sport] == 'Tennis') & (df[key_height] != np.nan)].drop_duplicates(subset=[key_name])[
        key_height])
q3_answer_mean = round(df_2000_women_tennis_heights.mean(), 1)
q3_answer_std = round(df_2000_women_tennis_heights.std(), 1)
print(f'Question 3 answer:\n  Mean: {q3_answer_mean}\n  Standard deviation: {q3_answer_std}\n{"-":-^100}')
worksheet4.write(2, 2, f'Среднее: {q1_answer_men}. Стандартное отклонение: {q1_answer_women}.')

worksheet4.write(3, 0, '4')
worksheet4.write(3, 1, 'Найдите спортсмена, участвовавшего в Олимпийских играх 2006 года, с самым высоким весом среди других участников '
                       'той же Олимпиады. Каким видом спорта он или она занимался?', text_wrap_format)
df_2006 = df[(df[key_year] == 2006) & (df[key_weight] != np.nan)]
row_max_weight = df_2006.loc[df_2006[key_weight].idxmax()]
q4_answer = row_max_weight[key_sport]
print(f'Question 4 answer: {q4_answer}\n{"-":-^100}')
worksheet4.write(3, 2, q4_answer)

worksheet4.write(4, 0, '5')
worksheet4.write(4, 1, 'Сколько раз Джон Ольберг участвовал в Олимпийских играх, проводимых в разные годы?')
df_john_aalberg = df[(df[key_name] == 'John Aalberg') & (df[key_year] != np.nan)].drop_duplicates(subset=[key_year])
q5_answer = len(df_john_aalberg)
print(f'Question 5 answer: {q5_answer}\n{"-":-^100}')
worksheet4.write(4, 2, q5_answer)

worksheet4.write(5, 0, '6')
worksheet4.write(5, 1, 'Сколько золотых медалей в теннисе завоевали спортсмены из сборной Швейцарии на Олимпийских играх 2008 года?')
df_2008_switzerland_tennis_gold = df[
    (df[key_year] == 2008) & (df[key_team] == 'Switzerland') & (df[key_sport] == 'Tennis') & (df[key_medal] == 'Gold')]
q6_answer = len(df_2008_switzerland_tennis_gold)
print(f'Question 6 answer: {q6_answer}\n{"-":-^100}')
worksheet4.write(5, 2, q6_answer)

worksheet4.write(6, 0, '7')
worksheet4.write(6, 1, 'Правда ли, что Испания завоевала меньше медалей, чем Италия на Олимпийских играх 2016 года?')
spain_2016_medals_count = len(df[(df[key_year] == 2016) & (df[key_medal] != np.nan) & (df[key_team] == 'Spain')])
italy_2016_medals_count = len(df[(df[key_year] == 2016) & (df[key_medal] != np.nan) & (df[key_team] == 'Italy')])
q7_answer = spain_2016_medals_count < italy_2016_medals_count
print(f'Question 7 answer: {q7_answer}\n{"-":-^100}')
worksheet4.write(6, 2, 'Да' if q7_answer else 'Нет')

worksheet4.write(7, 0, '8')
worksheet4.write(7, 1, 'К какой возрастной категории относилось наименьшее количество и наибольшее количество участников Олимпийских игр 2008 года?')
df_2008 = df[(df[key_year] == 2008) & (df[key_age] != np.nan)].drop_duplicates(subset=[key_name])
juniors_len = len(df_2008[df_2008[key_age] < 18])
adults_len = len(df_2008[(df_2008[key_age] >= 18) & (df_2008[key_age] < 35)])
veterans_len = len(df_2008[df_2008[key_age] >= 35])
switch_case_age_count = {
    juniors_len: ['juniors', 'Юниоры (до 18 лет)', juniors_len],
    adults_len: ['adults', 'Взрослые (от 19 до 35 лет)', adults_len],
    veterans_len: ['veterans', 'Ветераны (от 35 лет и старше)', veterans_len]
}
min_age_count = switch_case_age_count[min([juniors_len, adults_len, veterans_len])]
max_age_count = switch_case_age_count[max([juniors_len, adults_len, veterans_len])]
print(f'Question 8 answer:\n  Min count: {min_age_count[0]} ({min_age_count[2]})\n  Max count: {max_age_count[0]} ({max_age_count[2]})\n{"-":-^100}')
worksheet4.write(7, 2,
                 f'Наименьшее количество: {min_age_count[1]} ({min_age_count[2]}). Наибольшее количество: {max_age_count[1]} ({max_age_count[2]})')

worksheet4.write(8, 0, '9')
worksheet4.write(8, 1, 'Правда ли, что в Атланте проводились летние Олимпийские игры? '
                       'Правда ли, что в Скво-Вэлли проводились зимние Олимпийские игры?')
atlanta_summer = len(df[(df[key_city] == 'Atlanta') & ('Summer' in str(df[key_games]))]) > 0
squaw_valley_winter = len(df[(df[key_city] == 'Squaw Valley') & ('Winter' in str(df[key_games]))]) > 0
print(f'Question 9 answer:\n  Atlanta Summer: {atlanta_summer}\n  Squaw Valley Winter: {squaw_valley_winter}\n{"-":-^100}')
worksheet4.write(8, 2, f"Атланта летние игры: {'Да' if atlanta_summer else 'Нет'}. Скво-Вэлли зимние игры: {'Да' if squaw_valley_winter else 'Нет'}.")

worksheet4.write(9, 0, '10')
worksheet4.write(9, 1, 'Какова абсолютная разница между количеством уникальных видов спорта на Олимпийских играх 1986 года и '
                       'Олимпийских играх 2002 года?')
sports_count_1986 = len(df[df[key_year] == 1986].drop_duplicates(subset=[key_sport]))
sports_count_2002 = len(df[df[key_year] == 2002].drop_duplicates(subset=[key_sport]))
q10_answer = abs(sports_count_1986 - sports_count_2002)
print(f'Question 10 answer: {q10_answer}\n{"-":-^100}')
worksheet4.write(9, 2, q10_answer)

print('Wrote questions answers to xlsx')

################################################## PREDICTIVE MODEL ##################################################
print(f'\n\n{" PREDICTIVE MODEL ":-^100}')
# Prepare data
model_event = 'Swimming Women\'s 100 metres Freestyle'
model_sex = 'F'

df_prepared = pd.DataFrame(df[(df[key_event] == model_event) & (df[key_sex] == model_sex) & (
        df[key_medal] == 'Gold') & (df[key_year] != np.nan)].drop_duplicates(subset=[key_year]))
print(f'Event: {model_event}\nSex: {model_sex}\nRows before data preparation: {len(df_prepared[key_id])}')

df_prepared = prepare_date(df_prepared, key_age)
df_prepared = prepare_date(df_prepared, key_height)
df_prepared = prepare_date(df_prepared, key_weight)
df_prepared = df_prepared.sort_values([key_year])
print(f'Rows after data preparation: {len(df_prepared[key_id])}')

# Build model
year_pred = 2016

x_year = np.array(df_prepared[df_prepared[key_year] < year_pred][key_year].to_list()).reshape((-1, 1))
print(f'Years in prepared data set: {x_year.tolist()}\n{"-":-^100}')

worksheet5 = workbook.add_worksheet()
worksheet5.name = 'Prediction'

predict_value(df_prepared, key_age, year_pred, x_year, worksheet5, 0, 'A2')
predict_value(df_prepared, key_height, year_pred, x_year, worksheet5, 15, 'P2')
predict_value(df_prepared, key_weight, year_pred, x_year, worksheet5, 30, 'AE2')

print('Wrote prediction to xlsx')

workbook.close()