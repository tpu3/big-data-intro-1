FROM python:3.11

COPY athlete_events.csv /test/athlete_events.csv
COPY test.py /test/test.py
WORKDIR test

CMD [ "python", "test.py" ]